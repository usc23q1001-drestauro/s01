# Comments
# Comments in Python are done using the "#" symbol
# ctrl/cmd + / - comments in Python - one line comment

"""
although we have no keybind for
"""

# Python Syntax
# Hello World in Python

print("Hello World")

#Variables

age = 21
middle_initial = "L"
name1, name2, name3, name4 = "Daryll" , "Zyver" , "Jerald" , "Victor"

#Data Types
#strings
full_name = "Daryll Restauro"
secret_code = "Pa$$word"

#Numbers
num_of_days = 365 #this is an integer
pi_approx =  3.14 #this is a float
complex_num = 1 + 5j

#Boolean (bool) - for truth values

isLearning = True
isDifficult = False

#Using Variables
print("My name is " + full_name)
print("My age is " + str(age))

#Typecasting
#int() - converts the value to integer
#float() - converts the value to float
#str() - converts the value to string

print(int(3.5))
print(int("12322"))


#F-strings
print(f"My age is {age}")
print(f"My name is {full_name} and my age is {age}.")

#Operations
#Arithmetic

print(1+2)
print(18-9)
print(9-18)
print(3*2)
print(21/7)
print(23/7)
print(10%9)
print(2**5)

#Assignment Operators
num1 = 3
num1 +=4
print(num1)
print(1==2)